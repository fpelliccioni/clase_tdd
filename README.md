# Ejemplo de TDD para clase de Algoritmos III

Ejemplo de uso de TDD en C utilizando Catch2.

## Requerimientos

A modo de ejemplo implementaremos una librería con un TDA (Tipo de Data Abstracto) modelando el comportamiento de una tarjeta SUBE.
Nuestra librería debe permitir las siguientes operaciones:

* Registrar una nueva tarjeta SUBE, incluyendo el nombre del pasajero y su DNI. El saldo inicial debe ser $0.
* Consultar saldo disponible. Esta operación debe devolver solo el saldo real, sin considerar el descubierto, por lo que puede devolver un valor negativo.
* Consultar el descubierto disponible. El descubierto máximo es de $300, esta operación debe devolver el descubierto menos el saldo actual (si este fuera negativo).
* Cargar saldo. Esta operación permite acreditar un monto positivo al saldo de la tarjeta.
* Utilizar saldo. Esta operación permite utilizar el saldo disponible en un medio de transporte. Debe verificarse que la tarjeta tenga saldo suficiente para poder realizar el viaje y descontar el monto de ser así. Se debe considerar el descubierto disponible al momento de verificar si tiene el saldo para poder realizar el viaje.
